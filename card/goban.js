boardIdGenerator = 0

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function Goban(e, r, m) {
    this.boardId = 'goban' + (boardIdGenerator++).toString();
    e.attr('id', this.boardId);
    this.element = e;
    this.letters = "abcdefghijklmnopqrstuvwxyz";
    this.boardletters = "ABCDEFGHJKLMNOPQRST";
    this.currentNode = null;
    this.moveInSequence = 0;
    this.finished=false;
    this.touchEnabled=false;
    this.zoomed=false;
    this.rotation=r;
    this.mirror=m;

    this.size = 20;
    this.moveMarkerSize = 3;
    this.stonesize = 9;

    this.currentVariation = 0;
    this.currentBranchPt = 0;
    this.currentMove = 0;
}

Goban.prototype.setContent = function( sgf ) {
    this.sgf = sgf;
    var sz = parseInt( sgf.gameTrees[0].nodes[0].SZ );

    this.boardsize = {x:sz, y:sz};
    this.previousBoardState = {};
    this.boardstate = {};
    for(var i = 0; i < this.boardsize.x; i++) {
        this.boardstate[i+1] = {};
        this.previousBoardState[i+1] = {};
        for(var j = 0; j < this.boardsize.y; j++) {
            this.boardstate[i+1][j+1] = { x: i+1, y: j+1, pos: this.letters[i] + this.letters[j], stone:null, color:null, dead:null, deadmark:null };
            this.previousBoardState[i+1][j+1] = null;
        }
    }

    this.variationSequences = [];
    this.enumerateVariations( this.sgf.gameTrees[0], []);
}

Goban.prototype.enumerateVariations = function( currentNode, sequences ) {
    if('sequences' in currentNode) {
        for(var i in currentNode.sequences) {
            var nsequences = sequences.slice();
            nsequences.push(i);
            this.enumerateVariations( currentNode.sequences[i], nsequences );
        }
    }else {
        this.variationSequences.push( sequences );
    }
}

Goban.prototype.letter_to_grid = function(l){
    var acode = "a".charCodeAt(0);
    return l.charCodeAt(0) - acode + 1
}

Goban.prototype.grid_to_letter = function(g){
    var acode = "a".charCodeAt(0);
    return String.fromCharCode(g - 1 + acode);
}

Goban.prototype.parseBoolParam = function( v ) {
    if(typeof(v) === 'boolean')
        return v;
    else if(typeof(v) === 'number')
        return v == 1 ? true : 0;
    else if(typeof(v) === 'string')
        return v == "true" ? true : false;
    return false;
}

Goban.prototype.setParams = function(params) {
    this.fitWindow = false;
    this.showDead = false;
    this.markDead = false;
    this.enablePlay = false;
    this.enableNavigation = false;
    this.enableMask = true;

    this.xcrop=[ 1, this.boardsize.x ];
    this.ycrop=[ 1, this.boardsize.y ];

    if('fitWindow' in params)
        this.fitWindow = this.parseBoolParam( params.fitWindow );
    if('showDead' in params)
        this.showDead = this.parseBoolParam( params.showDead );
    if('markDead' in params)
        this.markDead = this.parseBoolParam( params.markDead );
    if('enablePlay' in params)
        this.enablePlay = this.parseBoolParam( params.enablePlay );
    if('enableNavigation' in params)
        this.enableNavigation = this.parseBoolParam( params.enableNavigation );
    if('enableMask' in params)
        this.enableMask = this.parseBoolParam( params.enableMask );


    if('size' in params) {
        this.size = parseInt(params.size);
        this.stonesize = this.size/2 - this.size/20;
        this.moveMarkerSize = this.size/10;
    }

    if('crop' in params) {
        var p1 = this.fromPosition( params.crop.substring(0,2) );
        var p2 = this.fromPosition( params.crop.substring(2,4) );

        this.xcrop = [p1.x, p2.x];
        if(p1.x > p2.x){
            this.xcrop = [p2.x, p1.x];
        }

        this.ycrop = [p1.y, p2.y];
        if(p1.y>p2.y){
            this.ycrop = [p2.y, p1.y];
        }
    }
}

Goban.prototype.fromGrid = function(pos) {
    var mx = this.size / 2;
    var my = this.size / 2;

    var pix = mx + (pos.x-1) * this.size;
    var piy = my + (pos.y-1) * this.size;
//    var piy = this.size * this.boardsize.y - (pos.y-1) * this.size - my;

    return {x:pix, y:piy};
}

Goban.prototype.fromPosition = function(pos) {
    pos = pos.toLowerCase();
    var szx = this.boardsize.x;
    var szy = this.boardsize.y;
    var gridx = pos.substring(0,1);
    var gridy = pos.substring(1,2);

    gridx = this.letter_to_grid(gridx);
    gridy = this.letter_to_grid(gridy);

    if(this.mirror==1){
        gridx=szx-gridx+1;
    }
    for(var i=0; i<this.rotation; i++){
        tmpx = gridx-1-(szx-1)/2
        tmpy = gridy-1-(szy-1)/2

        gridx=tmpy+(szy-1)/2+1
        gridy=-tmpx+(szx-1)/2+1
    }

    return {x:gridx, y:gridy};
}

Goban.prototype.toPosition = function(pos) {
    gridx = pos.x;
    gridy = pos.y;

    for(var i=0; i<this.rotation; i++){
        tmpx = gridx-1-(szx-1)/2
        tmpy = gridy-1-(szy-1)/2

        gridx=-tmpy+(szy-1)/2+1
        gridy=tmpx+(szx-1)/2+1
    }
    if(this.mirror==1){
        gridx=szx-gridx+1;
    }

    return this.grid_to_letter(gridx) + this.grid_to_letter(gridy);
}

Goban.prototype.removeStone = function(pos) {
    pos = this.fromPosition(pos);
    pix = this.fromGrid(pos);

    if(this.boardstate[pos.x][pos.y].stone != null) {
        this.boardstate[pos.x][pos.y].stone.remove();
        this.boardstate[pos.x][pos.y].stone = null;
        this.boardstate[pos.x][pos.y].color = null;
        this.boardstate[pos.x][pos.y].dead = false;
    }

    if(this.boardstate[pos.x][pos.y].deadmark != null) {
        this.boardstate[pos.x][pos.y].deadmark.remove();
        this.boardstate[pos.x][pos.y].deadmark = null;
    }

}

Goban.prototype.addStone = function(pos, color) {
    pos = this.fromPosition(pos);
    pix = this.fromGrid(pos);

    if(this.boardstate[pos.x][pos.y].dead) {
        if(this.showDead)
            this.boardstate[pos.x][pos.y].stone.remove();
        else if(this.markDead)
            this.boardstate[pos.x][pos.y].deadmark.remove();
    }

    var stone = this.stones.append('circle')
                    .attr('class', 'stone ' + color)
                    .attr('cx', pix.x)
                    .attr('cy', pix.y)
                    .attr('r', this.stonesize);

    this.boardstate[pos.x][pos.y].stone = stone;
    this.boardstate[pos.x][pos.y].color = color;
    this.boardstate[pos.x][pos.y].dead = false;
    this.boardstate[pos.x][pos.y].deadmark = null;
}

Goban.prototype.playPass = function(color, showMove) {
    if( color == 'b' )
        this.marquee.text("Black passed");
    else if( color == 'w' )
        this.marquee.text("White passed");
};

Goban.prototype.playStone = function(pos, color, showMove) {
    c = this.fromPosition(pos);

    var grps = [];
    if(c.x - 1 > 0)
        grps.push( this.findGroups(c.x-1, c.y) );
    if(c.x < this.boardsize.x)
        grps.push( this.findGroups(c.x+1, c.y) );
    if(c.y - 1 > 0)
        grps.push( this.findGroups(c.x, c.y-1) );
    if(c.y < this.boardsize.y)
        grps.push( this.findGroups(c.x, c.y+1) );

    for(var i in grps) {
        if(grps[i].stones.length == 0) continue;

        var g_liberties = this.countLiberties( grps[i].stones );

        if(g_liberties == 1 && grps[i].color != color) {
            this.killGroup( grps[i] );
        }
    }

    this.addStone(pos, color);

    if(showMove)
        this.moveMarker.style('visibility', 'visible')
                        .attr('x', (c.x - 0.5) * this.size - this.moveMarkerSize)
                        .attr('y', (c.y - 0.5) * this.size - this.moveMarkerSize);
};

function uniqueValues( arr ){
    var s = {};
    var u = [];

    for(var i in arr) {
        if( !(arr[i] in s) ) {
            s[ arr[i] ] = 1;
            u.push( arr[i] );
        }
    }

    return u;
};

Goban.prototype.countLiberties = function(stones) {
    var liberties = [];
    var bx = this.boardsize.x;
    var by = this.boardsize.y;

    for(var i in stones){
        var st = stones[i];
        if(st.x > 1  && !this.hasStone(st.x-1, st.y)) liberties.push( this.boardstate[st.x-1][st.y].pos );
        if(st.x < bx && !this.hasStone(st.x+1, st.y)) liberties.push( this.boardstate[st.x+1][st.y].pos );
        if(st.y > 1  && !this.hasStone(st.x, st.y-1)) liberties.push( this.boardstate[st.x][st.y-1].pos );
        if(st.y < by && !this.hasStone(st.x, st.y+1)) liberties.push( this.boardstate[st.x][st.y+1].pos );
    }

    return uniqueValues( liberties ).length;
};

Goban.prototype.hasStone = function(x, y) {
    var st = this.boardstate[x][y];
    return st.color != null && !st.dead;
}

Goban.prototype.killGroup = function( g ) {
    for(var i in g.stones){
        this.killStone( g.stones[i].x, g.stones[i].y );
    }
};

Goban.prototype.killStone = function(x, y) {
    var st = this.boardstate[ x ][ y ];
    st.dead = true;

    if(this.showDead) {
        st.stone.classed('dead', true);
    } else if(this.markDead) {
        var mx = this.stonesize / 1.1;
        var pix = this.fromGrid( {'x':x, 'y':y} );

        var color = 'w';
        if(color == st.color) color = 'b';

        st.deadmark = this.stones.append('g').attr('transform', 'translate('+pix.x+','+pix.y+')');
        st.deadmark.append('line')
                        .attr('class', 'MA dead ' + color)
                        .attr('x1', -mx)
                        .attr('y1', -mx)
                        .attr('x2', mx)
                        .attr('y2', mx);

        st.deadmark.append('line')
                        .attr('class', 'MA dead ' + color)
                        .attr('x1', -mx)
                        .attr('y1', mx)
                        .attr('x2', mx)
                        .attr('y2', -mx);
    } else {
        st.color = null;
        st.stone.remove();
        st.stone = null;
        st.dead = false;
        st.deadmark = null;
    } 
};

Goban.prototype.findGroups = function(x, y, c, g, v) {
    if(typeof c == 'undefined') c = this.boardstate[x][y].color;
    if(typeof g == 'undefined') g = {stones:[], color:c};
    if(typeof v == 'undefined') v = {};

    var bx = this.boardsize.x;
    var by = this.boardsize.y;

    var st = this.boardstate[x][y];
    if( !(st.pos in v) && st.color == c && this.hasStone(x, y) ) {
        v[st.pos] = 1;
        g.stones.push( st );

        if(x > 1)  g = this.findGroups(x-1, y, c, g, v);
        if(x < bx) g = this.findGroups(x+1, y, c, g, v);
        if(y > 1)  g = this.findGroups(x, y-1, c, g, v);
        if(y < by) g = this.findGroups(x, y+1, c, g, v);
    }

    return g;
};

Goban.prototype.maskMarkup = function( pix ) {
    if( this.enableMask ) {
        this.mask.append("svg:circle")
              .style('fill', 'black')
              .attr('cx', pix.x)
              .attr('cy', pix.y)
              .attr('r', this.stonesize / 1.1);
    }
};

Goban.prototype.markupLB = function(pos, lbl) {
    var grid = this.fromPosition(pos);
    var pix = this.fromGrid(grid);
    var mx = this.size / 1.5;

    var color = 'b';
    if(this.boardstate[grid.x][grid.y].color == 'b') {
        color = 'w';
    }

    this.markup.append('text')
                    .attr('class', 'LB ' + color)
                    .attr('x', pix.x)
                    .attr('y', pix.y)
                    .style('font-size', mx + "px")
                    .text(lbl);
    this.maskMarkup( pix );
};

Goban.prototype.markupTR = function(pos) {
    var grid = this.fromPosition(pos);
    var pix = this.fromGrid(grid);
    var r = this.stonesize*0.66;

    var color = 'b';
    if(this.boardstate[grid.x][grid.y].color == 'b') {
        color = 'w';
    }

    var pt1 = r*Math.cos(-Math.PI/6) + "," + -r*Math.sin(-Math.PI/6);
    var pt2 = r*Math.cos(-Math.PI+Math.PI/6) + "," + -r*Math.sin(-Math.PI+Math.PI/6);
    var pt3 = r*Math.cos(Math.PI/2) + "," + -r*Math.sin(Math.PI/2);
    var transform = "translate("+pix.x+","+pix.y+")";

    this.markup.append('polygon')
                    .attr('class', 'TR ' + color)
                    .attr('points', pt1 + " " + pt2 + " " + pt3)
                    .attr('transform', transform);
    this.maskMarkup( pix );
};

Goban.prototype.markupSQ = function(pos) {
    var grid = this.fromPosition(pos);
    var pix = this.fromGrid(grid);
    var mx = this.stonesize / 2;

    var color = 'b';
    if(this.boardstate[grid.x][grid.y].color == 'b') {
        color = 'w';
    }

    this.markup.append('rect')
                    .attr('class', 'SQ ' + color)
                    .attr('x', pix.x - mx)
                    .attr('y', pix.y - mx)
                    .attr('width', mx * 2)
                    .attr('height', mx * 2);
    this.maskMarkup( pix );
};

Goban.prototype.markupCR = function(pos) {
    var grid = this.fromPosition(pos);
    var pix = this.fromGrid(grid);
    var mx = this.stonesize / 3.5;

    var color = 'b';
    if(this.boardstate[grid.x][grid.y].color == 'b') {
        color = 'w';
    }

    this.markup.append('circle')
                    .attr('class', 'CR ' + color)
                    .attr('cx', pix.x)
                    .attr('cy', pix.y)
                    .attr('r', mx * 2);
    this.maskMarkup( pix );
};

Goban.prototype.markupMA = function(pos) {
    var grid = this.fromPosition(pos);
    var pix = this.fromGrid(grid);
    var mx = this.stonesize / 2;

    var color = 'b';
    if(this.boardstate[grid.x][grid.y].color == 'b') {
        color = 'w';
    }

    this.markup.append('line')
                    .attr('class', 'MA ' + color)
                    .attr('x1', pix.x-mx)
                    .attr('y1', pix.y-mx)
                    .attr('x2', pix.x+mx)
                    .attr('y2', pix.y+mx);

    this.markup.append('line')
                    .attr('class', 'MA ' + color)
                    .attr('x1', pix.x+mx)
                    .attr('y1', pix.y-mx)
                    .attr('x2', pix.x-mx)
                    .attr('y2', pix.y+mx);
    this.maskMarkup( pix );
};

Goban.prototype.addStar = function(pos) {
    var grid = this.fromPosition(pos);
    var pix = this.fromGrid(grid);

    this.lines.append('circle')
                    .attr('class', 'star')
                    .attr('cx', pix.x)
                    .attr('cy', pix.y)
                    .attr('r', this.stonesize/3);
};

Goban.prototype.show = function() {
    this.makeMarquee();
    this.makeBoard();

    if(this.enableNavigation) {
        this.makeNavigationToolbar();
        this.makeNavModeMarker();
    }else if(this.enablePlay) {
        this.makeInteractive();
        this.makePlayModeMarker();
    }else{
        this.makeQuestionModeMarker();
    }

    this.makeCommentary();
};

Goban.prototype.makeMarquee = function() {
    this.marquee = this.element.append("div")
                                .attr("class", "marquee");
};

Goban.prototype.handleClick = function(e, d) {
    if( this.touchEnabled ){
        if( this.zoomed && this.doPlayStone( d ) ) {
            var goban = this;
            setTimeout(function() {
                goban.zoomOut();
            }, 50);
        } else {
            this.zoomIn( d );
        }
    }else{
        this.doPlayStone( d );
    }
};

Goban.prototype.zoomIn = function(d) {
    this.zoomed = true;
    var zTransform = this.getZoomTransform(d);
    this.yaxis.selectAll("text.label").attr('transform', "scale(1,0.5)");
    this.xaxis.selectAll("text.label").attr('transform', "scale(0.5,1)");

    this.goban.attr('transform', zTransform.goban);
    this.xaxis.attr('transform', zTransform.xaxis);
    this.yaxis.attr('transform', zTransform.yaxis);
};

Goban.prototype.zoomOut = function() {
    this.zoomed = false;
    this.goban.attr('transform', this.cropTransform.goban);
    this.xaxis.attr('transform', this.cropTransform.xaxis);
    this.yaxis.attr('transform', this.cropTransform.yaxis);

    this.yaxis.selectAll("text.label").attr('transform', "scale(1,1)");
    this.xaxis.selectAll("text.label").attr('transform', "scale(1,1)");
};

Goban.prototype.getZoomTransform = function(d) {
    var sc = 2;

    var xt = -( d.x - (this.xcrop[1] - this.xcrop[0])/2 - 1 ) * this.size * sc;
    var yt = -( d.y - (this.ycrop[1] - this.ycrop[0])/2 - 1 ) * this.size * sc;

    var xt2 = -sc * this.size;
    var yt2 = -sc * this.size;

    var translate = "translate("+xt+","+yt+")";
    var scale = "scale("+sc+","+sc+")";
    var translate2 = "translate("+xt2+","+yt2+")";
    var transform = { goban: translate + scale + translate2,
                      xaxis: "translate("+xt+",0)scale("+sc+",1)translate("+xt2+",0)",
                      yaxis: "translate(0,"+yt+")scale(1,"+sc+")translate(0,"+yt2+")"
                        };
    return transform;
};


Goban.prototype.doPlayStone = function(d) {
    var played = false;
    var goban = this;
    var moves = this.getNextMoves( );

    for(var i in moves){
        var dest = moves[i];
        if(dest.move.pos == d.pos){
            this.executeMove( dest.treeNode, dest.moveInSequence, true );
            setTimeout( function(){ goban.processOpponent(); }, 200 );
            return true;
        }
    }

    if(moves.length > 0) {
        this.finished = true;
        this.correct = false;
    }

    var color = this.getPlayer();

    if( !this.hasStone( d.x, d.y ) && this.canPlay( d, color ) && !this.violatesKo( d, color ) ) {
        var node = { W: d.pos };
        if(color == 'b')
            var node = { B: d.pos };

        this.executeMove( {'nodes':[node]}, 0, true );
        played = true;
    }

    if(this.finished && played){
        if(this.correct)
            this.marquee.text("Correct");
        else
            this.marquee.text("Incorrect");
    }

    return played;
};

Goban.prototype.violatesKo = function( c, color ) {
    var nBoardState = this.copyBoardState();

    var grps = [];
    if(c.x - 1 > 0)
        grps.push( this.findGroups(c.x-1, c.y) );
    if(c.x < this.boardsize.x)
        grps.push( this.findGroups(c.x+1, c.y) );
    if(c.y - 1 > 0)
        grps.push( this.findGroups(c.x, c.y-1) );
    if(c.y < this.boardsize.y)
        grps.push( this.findGroups(c.x, c.y+1) );

    var liberties = this.countLiberties( [ c ] );
    nBoardState[c.x][c.y] = color;

    if( liberties == 0 ) {
        for(var i in grps) {
            if(grps[i].stones.length == 0) continue;

            var g_liberties = this.countLiberties( grps[i].stones );

            if(g_liberties == 1 && grps[i].color != color) {
                for(var j in grps[i].stones) {
                    var x = grps[i].stones[j].x;
                    var y = grps[i].stones[j].y;
                    nBoardState[x][y] = null;
                }
            }
        }
    }

//    this.printBoardState( nBoardState );
    for( var i=1; i <= this.boardsize.x; i++ ) {
        for( var j=1; j <= this.boardsize.y; j++ ) {
            if( nBoardState[i][j] != this.previousBoardState[i][j] )
                return false;
        }
    }
    return true;
};

Goban.prototype.printBoardState = function( bState ) {
    var p = "";

    for( var i=1; i <= this.boardsize.x; i++ ) {
        var b1 = "";
        var b2 = "";

        for( var j=1; j <= this.boardsize.y; j++ ) {
            if( bState[i][j] === null )
                b1 += " ";
            else
                b1 += bState[i][j];

            if( this.previousBoardState[i][j] === null )
                b2 += " ";
            else
                b2 += this.previousBoardState[i][j];
        }


        p += b1 + " " + b2 + "\n";
    }

    console.log( p );
}


Goban.prototype.canPlay = function( c, color ) {
    var grps = [];
    if(c.x - 1 > 0)
        grps.push( this.findGroups(c.x-1, c.y) );
    if(c.x < this.boardsize.x)
        grps.push( this.findGroups(c.x+1, c.y) );
    if(c.y - 1 > 0)
        grps.push( this.findGroups(c.x, c.y-1) );
    if(c.y < this.boardsize.y)
        grps.push( this.findGroups(c.x, c.y+1) );

    var liberties = this.countLiberties( [ c ] );
    if( liberties == 0 ) {
        for(var i in grps) {
            if(grps[i].stones.length == 0) continue;

            var g_liberties = this.countLiberties( grps[i].stones );

            if(g_liberties == 1 && grps[i].color != color) {
                return true;
            }
            if(g_liberties > 1 && grps[i].color == color) {
                return true;
            }
        }
        return false;
    }
    return true;
};

Goban.prototype.getPlayer = function() {
    var node = this.currentNode.nodes[this.moveInSequence];

    if('B' in node) return 'w';
    if('W' in node) return 'b';
    if('PL' in node) return node.PL.toLowerCase();

    return null;
};

Goban.prototype.processOpponent = function( ) {
    var moves = this.getNextMoves( );
    var i = 0;
    if(moves.length > 1)
        i = Math.floor(Math.random() * moves.length);
    if(moves.length > 0) {
        this.executeMove( moves[i].treeNode, moves[i].moveInSequence, true );
        moves = this.getNextMoves( );
    } else {
        this.marquee.text("Correct");
        this.finished = true;
        this.correct = true;
    }
    if(!this.finished && moves.length == 0) {
        this.marquee.text("Incorrect");
        this.finished = true;
        this.correct = false;
    }
};

Goban.prototype.getNextMoves = function() {
    if( this.currentNode.nodes.length > this.moveInSequence + 1 ) {
        var move = this.getMove( this.currentNode.nodes[this.moveInSequence+1] );
        var dest = {'move':move, 'treeNode':this.currentNode, 'moveInSequence':this.moveInSequence+1 };
        return [ dest ];
    } else if( 'sequences' in this.currentNode ) {
        var moves = [];
        for( var i in this.currentNode.sequences ) {
            var subtree = this.currentNode.sequences[i];
            var move = this.getMove( subtree.nodes[0] );
            var dest = {'move':move, 'treeNode':subtree, 'moveInSequence':0 };
            moves.push( dest );
        }
        return moves;
    }
    return [];
};

Goban.prototype.getMove = function( node ) {
    if ('B' in node) {
        if( Object.prototype.toString.call( node.B ) === '[object Array]' )
            return {'color':'b', 'pos':node.B[0]};
        else
            return {'color':'b', 'pos':node.B};
    }
    if ('W' in node) {
        if( Object.prototype.toString.call( node.W ) === '[object Array]' )
            return {'color':'w', 'pos':node.W[0]};
        else
            return {'color':'w', 'pos':node.W};
    }
    return { 'color':null, 'pos':'pass' };
};

Goban.prototype.makeNavModeMarker = function() {
    this.svg.append('circle')
                    .attr('class', 'stone w')
                    .attr('cx', this.size/2)
                    .attr('cy', this.size/2)
                    .attr('r', this.size/3)
    this.svg.append('text')
                    .attr('class', 'label')
                    .attr('x', this.size/2)
                    .attr('y', this.size/2)
                    .style('alignment-baseline', "central")
                    .style('font-size', this.size/2.2)
                    .text("V");
};

Goban.prototype.makeQuestionModeMarker = function() {
    this.svg.append('circle')
                    .attr('class', 'stone w')
                    .attr('cx', this.size/2)
                    .attr('cy', this.size/2)
                    .attr('r', this.size/3)
    this.svg.append('text')
                    .attr('class', 'label')
                    .attr('x', this.size/2)
                    .attr('y', this.size/2)
                    .style('alignment-baseline', "central")
                    .style('font-size', this.size/2.2)
                    .text("?");
};

Goban.prototype.makePlayModeMarker = function() {
    this.svg.append('circle')
                    .attr('class', 'stone b')
                    .attr('cx', this.size/2 - this.size/5)
                    .attr('cy', this.size/2 - this.size/5)
                    .attr('r', this.size/5);
    this.svg.append('circle')
                    .attr('class', 'stone w')
                    .attr('cx', this.size/2 + this.size/5)
                    .attr('cy', this.size/2 + this.size/5)
                    .attr('r', this.size/5);
    this.svg.append('line')
                    .attr('class', 'line')
                    .style('stroke-width', '1px')
                    .attr('x1', this.size / 2 - this.size/3)
                    .attr('y1', this.size / 2 + this.size/3)
                    .attr('x2', this.size / 2 + this.size/3)
                    .attr('y2', this.size / 2 - this.size/3);
};

Goban.prototype.makeInteractive = function() {
    var xt = -( this.xcrop[0] - 2 ) * this.size;
    var yt = -( this.ycrop[0] - 2 ) * this.size;
    var goban = this;

    this.element.on('touchstart',
                        function(){
                            goban.touchEnabled=true;
                        });

    this.element.on('click',
                        function(){
                            if(goban.zoomed) {
                                goban.zoomOut();
                            }
                        });
    this.detectors = this.goban.append("g");

    for(var i = this.xcrop[0]; i <= this.xcrop[1]; i++) {
        for(var j = this.ycrop[0]; j <= this.ycrop[1]; j++) {
            var grid = {x:i, y:j};
            var pix = this.fromGrid( grid );
            var pos = this.toPosition( grid );

            this.detectors.append('rect')
                            .data([ {x:grid.x, y:grid.y, pos:pos} ])
                            .attr('class', 'detector')
                            .attr('x', pix.x - this.stonesize)
                            .attr('y', pix.y - this.stonesize)
                            .attr('width', this.stonesize*2)
                            .attr('height', this.stonesize*2)
                            .on('click', function(d) {
                                d3.event.stopPropagation();
                                goban.handleClick( d3.select(this), d );
                            });
        }
    }

    var cwidth = 600;
    if(this.winWidth < cwidth)
        cwidth = this.winWidth;
    this.playbar = this.element.append("div")
                                .attr("class", "navigation")
                                .style('width', cwidth+"px");
    this.playbar.append('div')
                    .attr('class', "button")
                    .text("<<")
                    .on('click', function() {
                        goban.toStart();
                    });
};

Goban.prototype.makeNavigationToolbar = function() {
    var cwidth = 600;
    if(this.winWidth < cwidth)
        cwidth = this.winWidth;
    this.moveNavigator = this.element.append("div")
                                        .attr("class", "navigation")
                                        .style('width', cwidth+"px");

    this.moveNavigator.append('div')
                        .attr('class', "button")
                        .text("<<")
                        .on('click', function() {
                            goban.toStart();
                        });
    this.moveNavigator.append('div')
                        .attr('class', "button")
                        .text("<")
                        .on('click', function() {
                            goban.prevMove();
                        });
    this.moveNavigator.append('div')
                        .attr('class', "button")
                        .text(">")
                        .on('click', function() {
                            goban.nextMove();
                        });
    this.moveNavigator.append('div')
                        .attr('class', "button")
                        .text(">>")
                        .on('click', function() {
                            goban.toEnd();
                        });

    this.variationSelector = this.element.append("div")
                                            .attr("class", "variations")
                                            .style('width', cwidth+"px");
    this.variationSelector.append("div")
            .attr('class', 'header')
            .text("V");


    var goban = this;
    for(var i in this.variationSequences){
        var txt = "M";
        var cls = "variant";
        if(i > 0) txt = "" + i;
        if(i == this.currentVariation) cls += " selected";

        this.variationSelector.append("div")
                                .attr("id", "var" + i)
                                .attr("class", cls)
                                .on('click', function(){ 
                                    var varNum = parseInt(d3.select(this).attr('id').substring(3));
                                    goban.playVariation( varNum );
                                })
                                .text(txt);
    }
};

Goban.prototype.playVariation = function( varNum ){
    this.currentVariation = varNum;

    for(var i in this.variationSequences) {
        var cls = "variant";
        if(i == varNum) {
            cls += " selected";
        }

        this.varSelector = d3.select("#var" + i)
                                .attr('class', cls);
    }

    this.toStart();
    while(this.nextMove()){}
}

Goban.prototype.makeCommentary = function() {
    var cwidth = 600;
    if(this.winWidth < cwidth)
        cwidth = this.winWidth;
    this.commentary = this.element.append("div")
                                    .attr("class", "commentary")
                                    .style("width", cwidth+"px");
}

Goban.prototype.makeBoard = function() {
    if(this.fitWindow) {
        var bwidth = this.xcrop[1] - this.xcrop[0] + 3;
        var bheight = this.ycrop[1] - this.ycrop[0] + 2;
        var w = window.innerWidth;
        var h = window.innerHeight - 100;

        this.size = w / bwidth;
        if(this.size * bheight > h)
            this.size = h / bheight;

        this.winWidth = w - this.size;

        this.moveMarkerSize = this.size/10;
        this.stonesize = this.size/2 - this.size/20;
    }


    this.xsize = (this.xcrop[1] - this.xcrop[0] + 3) * this.size;
    this.ysize = (this.ycrop[1] - this.ycrop[0] + 2) * this.size;

    this.svg = this.element.append("svg")
                .attr('width', this.xsize)
                .attr('height', this.ysize);

    if( this.enableMask ) {
        this.maskSvg = this.element.append("svg")
                        .attr('id', 'markup-mask-svg')
                        .attr('width', this.xsize)
                        .attr('height', this.ysize);
        this.mask = this.maskSvg.append("svg:defs").append("svg:mask").attr("id", "markup-mask");
    }


    var xt = -( this.xcrop[0] - 2 ) * this.size;
    var yt = -( this.ycrop[0] - 2 ) * this.size;

    this.cropTransform = {  xaxis:'translate('+xt+',0)',
                            yaxis:'translate(0,'+yt+')',
                            goban:'translate('+xt+','+yt+')' };

    this.goban = this.svg.append("g")
                                .attr('transform', this.cropTransform.goban);
    this.wood = this.goban.append("g");
    this.lines = this.goban.append("g")
    if( this.enableMask ) {
        this.lines.attr("mask", function(d,i) { return "url(#markup-mask)"; });
    }

    this.xaxis = this.svg.append("g")
                                .attr('class', 'xaxis')
                                .attr('transform', this.cropTransform.xaxis);

    this.yaxis = this.svg.append("g")
                                .attr('class', 'yaxis')
                                .attr('transform', this.cropTransform.yaxis);


    this.yaxis.append("rect")
                        .attr('class', 'background')
                        .attr('x', 0)
                        .attr('y', -this.size)
                        .attr('width', this.size)
                        .attr('height', (this.boardsize.y+1) * this.size);

    this.yaxis.append("rect")
                        .attr('class', 'background')
                        .attr('x', (this.xcrop[1] - this.xcrop[0] + 2) * this.size)
                        .attr('y', -this.size)
                        .attr('width', this.size)
                        .attr('height', (this.boardsize.y+1) * this.size);

    this.xaxis.append("rect")
                        .attr('class', 'background')
                        .attr('x', 0)
                        .attr('y', 0)
                        .attr('width', (this.boardsize.x) * this.size)
                        .attr('height', this.size);

    this.corner = this.svg.append("rect")
                        .attr('class', 'background')
                        .attr('x', 0)
                        .attr('y', 0)
                        .attr('width', this.size)
                        .attr('height', this.size);

    var mx = this.size / 2;
    var my = this.size / 2;

    var topleft = this.fromGrid({x:1, y:1});
    var botright = this.fromGrid(this.boardsize);

    this.wood.append('rect')
                    .attr('x', topleft.x)
                    .attr('y', topleft.y)
                    .attr('width', botright.x - topleft.x)
                    .attr('height', botright.y - topleft.y)
                    .attr('class', 'goban');

    if( this.enableMask ) {
        this.mask.append("svg:rect")
                        .attr('x', topleft.x)
                        .attr('y', topleft.y)
                        .attr('width', botright.x - topleft.x)
                        .attr('height', botright.y - topleft.y)
                        .style('fill', 'white');
    }

    this.wood.append('image')
                    .attr('x', topleft.x-mx)
                    .attr('y', topleft.y-my)
                    .attr('width', botright.x - topleft.x + 2 * mx)
                    .attr('height', botright.y - topleft.y + 2 * my)
                    .attr('preserveAspectRatio', "xMinYMin slice")
                    .attr('xlink:href', 'WoodGrain.jpg')
                    .attr('class', 'goban');

    this.lines.append('rect')
                    .attr('x', topleft.x)
                    .attr('y', topleft.y)
                    .attr('width', botright.x - topleft.x)
                    .attr('height', botright.y - topleft.y)
                    .attr('class', 'border');


    if(this.boardsize.x == 19 && this.boardsize.y == 19) {
        this.addStar('dd');
        this.addStar('dj');
        this.addStar('dp');

        this.addStar('jd');
        this.addStar('jj');
        this.addStar('jp');

        this.addStar('pd');
        this.addStar('pj');
        this.addStar('pp');
    }
    if(this.boardsize.x == 13 && this.boardsize.y == 13) {
        this.addStar('dd');
        this.addStar('dg');
        this.addStar('dj');

        this.addStar('gd');
        this.addStar('gg');
        this.addStar('gj');

        this.addStar('jd');
        this.addStar('jg');
        this.addStar('jj');
    }
    if(this.boardsize.x == 9 && this.boardsize.y == 9) {
        this.addStar('cc');
        this.addStar('cg');
        this.addStar('gc');
        this.addStar('gg');
        this.addStar('ee');
    }

    for(var i = 0; i < this.boardsize.y; i++){
        var lpos = this.fromGrid({x:1, y:i+1});
        var rpos = this.fromGrid({x:this.boardsize.x, y:i+1});

        this.lines.append('line')
                .attr('class', 'line')
                .attr('x1', lpos.x)
                .attr('x2', rpos.x)
                .attr('y1', lpos.y)
                .attr('y2', rpos.y);

        this.yaxis.append('g')
                    .attr('transform', 'translate('+mx+','+lpos.y+')')
                    .append("text")
                        .attr('class', 'label')
                        .style('font-size', mx*1.2 + "px")
                        .text((i+1).toString());
    }
    for(var i = 0; i < this.boardsize.x; i++){
        var lpos = this.fromGrid({x:i+1, y:1});
        var rpos = this.fromGrid({x:i+1, y:this.boardsize.y});

        this.lines.append('line')
                .attr('class', 'line')
                .attr('x1', lpos.x)
                .attr('x2', rpos.x)
                .attr('y1', lpos.y)
                .attr('y2', rpos.y);

        this.xaxis.append('g')
                    .attr('transform', 'translate('+lpos.x+','+my+')')
                    .append('text')
                        .attr('class', 'label')
                        .style('font-size', mx*1.2 + "px")
                        .text(this.boardletters[i]);
    }
    this.stones = this.goban.append("g");
    this.markup = this.goban.append("g");
    this.moveMarker = this.goban.append("rect")
                                .attr('class', 'movemarker')
                                .style('visibility', 'hidden')
                                .attr('x', 0)
                                .attr('y', 0)
                                .attr('width', this.moveMarkerSize*2)
                                .attr('height', this.moveMarkerSize*2);

}

Goban.prototype.copyBoardState = function() {
    var pBoardState = {};

    for(var i=1; i <= this.boardsize.x; i++){
        pBoardState[i] = {};
        for(var j=1; j <= this.boardsize.y; j++){
            if(this.hasStone(i,j))
                pBoardState[i][j] = this.boardstate[i][j].color
            else
                pBoardState[i][j] = null;
        }
    }

    return pBoardState;
};

Goban.prototype.executeMove = function( treeNode, number, showMove ) {
    this.markup.selectAll("*").remove();
    if( this.enableMask ) {
        this.mask.selectAll("circle").remove();
    }
    this.currentNode = treeNode;
    this.moveInSequence = number;
    this.currentMove++;

    var commands = treeNode.nodes[number];
    this.previousBoardState = this.copyBoardState();

    this.moveMarker.style('visibility', 'hidden');
    if('AB' in commands) {
        if( Object.prototype.toString.call( commands.AB ) === '[object Array]' ) {
            for(var i in commands.AB){
                this.addStone(commands.AB[i], 'b');
            }
        }else{
            this.addStone(commands.AB, 'b');
        }
    }
    if('AW' in commands) {
        if( Object.prototype.toString.call( commands.AW ) === '[object Array]' ) {
            for(var i in commands.AW){
                this.addStone(commands.AW[i], 'w');
            }
        }else{
            this.addStone(commands.AW, 'w');
        }
    }
    if('AE' in commands) {
        if( Object.prototype.toString.call( commands.AE ) === '[object Array]' ) {
            for(var i in commands.AE){
                this.removeStone(commands.AE[i]);
            }
        }else{
            this.removeStone(commands.AE);
        }
    }
    if('B' in commands) {
        if(commands.B == "" || commands.B == '``' || commands.B == 'tt')
            this.playPass('b', showMove);
        else
            this.playStone(commands.B, 'b', showMove);
    }
    if('W' in commands) {
        if(commands.W == "" || commands.W == '``' || commands.W == 'tt')
            this.playPass('w', showMove)
        else
            this.playStone(commands.W, 'w', showMove);
    }
    if('LB' in commands) {
        var processLabel = function(lblcmd, goban) {
            lblcmd = lblcmd.split(":");
            var pos = lblcmd[0];
            var lbl = lblcmd[1];

            goban.markupLB(pos, lbl);
        }

        if( Object.prototype.toString.call( commands.LB ) === '[object Array]' ) {
            for(var i in commands.LB){
                processLabel(commands.LB[i], this);
            }
        }else{
            processLabel(commands.LB, this);
        }
    }
    if('SQ' in commands) {
        if( Object.prototype.toString.call( commands.SQ ) === '[object Array]' ) {
            for(var i in commands.SQ){
                this.markupSQ(commands.SQ[i]);
            }
        }else{
            this.markupSQ(commands.SQ);
        }
    }
    if('CR' in commands) {
        if( Object.prototype.toString.call( commands.CR ) === '[object Array]' ) {
            for(var i in commands.CR){
                this.markupCR(commands.CR[i]);
            }
        }else{
            this.markupCR(commands.CR);
        }
    }
    if('TR' in commands) {
        if( Object.prototype.toString.call( commands.TR ) === '[object Array]' ) {
            for(var i in commands.TR){
                this.markupTR(commands.TR[i]);
            }
        }else{
            this.markupTR(commands.TR);
        }
    }
    if('MA' in commands) {
        if( Object.prototype.toString.call( commands.MA ) === '[object Array]' ) {
            for(var i in commands.MA){
                this.markupMA(commands.MA[i]);
            }
        }else{
            this.markupMA(commands.MA);
        }
    }
    if('C' in commands) {
        this.commentary.selectAll("*").remove();
        var lines = commands.C.split("\n");
        for(var i in lines){
            this.commentary.append("div").text(lines[i]);
        }
    }else{
        this.commentary.selectAll("*").remove();
    }

    this.marquee.text("");
    if(this.currentMove == 1) {
        if('PL' in commands) {
            var PL = commands.PL;
            if( Object.prototype.toString.call( PL ) === '[object Array]' ) {
                PL = PL[PL.length - 1];
            }
            if(PL == 'b' || PL == 'B')
                this.marquee.text('Black to play');
            if(PL == 'w' || PL == 'W')
                this.marquee.text('White to play');
        } else {
            nxtCmd = this.getNextNode();
            if('B' in nxtCmd) {
                this.marquee.text('Black to play');
            } else if('W' in nxtCmd) {
                this.marquee.text('White to play');
            }
        }
    }
}

Goban.prototype.clear = function() {
    this.svg.remove();
}

Goban.prototype.clearBoard = function() {
    this.stones.selectAll("*").remove();
    this.markup.selectAll("*").remove();
    if( this.enableMask ) {
        this.mask.selectAll("circle").remove();
    }
    this.moveMarker.style('visibility', 'hidden');

    for(var i = 0; i < this.boardsize.x; i++) {
        for(var j = 0; j < this.boardsize.y; j++) {
            this.boardstate[i+1][j+1].stone = null;
            this.boardstate[i+1][j+1].color = null;
            this.boardstate[i+1][j+1].dead = false;
            this.boardstate[i+1][j+1].deadmark = null;

            this.previousBoardState[i+1][j+1] = null;
        }
    }
}

Goban.prototype.toStart = function() {
    this.clearBoard();
    this.currentMove = 0;
    this.currentBranchPt = 0;
    this.finished=false;
    this.executeMove(this.sgf.gameTrees[0], 0, false);
}

Goban.prototype.toEnd = function() {
    while(this.nextMove()){}
}

Goban.prototype.prevMove = function() {
    if(this.currentMove > 0)
        this.toMove( this.currentMove - 1 )
}

Goban.prototype.toMove = function(moveNum) {
    this.toStart();
    for(var i = 1; i < moveNum; i++) {
        this.nextMove()
    }
}

Goban.prototype.nextMove = function() {
    var nodeLen = this.currentNode.nodes.length;

    if(this.moveInSequence + 1 < nodeLen) {
        this.executeMove(this.currentNode, this.moveInSequence+1, false);
        return true;
    } else if('sequences' in this.currentNode) {
        var seq = this.variationSequences[this.currentVariation][this.currentBranchPt];
        this.executeMove(this.currentNode.sequences[seq], 0, false);
        this.currentBranchPt++;
        return true;
    }
    return false;
}

Goban.prototype.getNextNode = function() {
    console.log( this.currentMove );
    console.log( this.moveInSequence );
    console.log( this.currentNode );
    if(this.moveInSequence+1==this.currentNode.nodes.length){
        var seq = this.variationSequences[this.currentVariation][this.currentBranchPt];
        return this.currentNode.sequences[seq].nodes[0];
    } else {
        return this.currentNode.nodes[this.moveInSequence+1];
    }
}
