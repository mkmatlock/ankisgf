#!/usr/bin/python
import sys


def read_content(fn):
    with open(fn, 'r') as ffile:
        content = ""
        for line in ffile:
            content+=line
        return content

sgf_html = read_content( 'sgf.html' )

if '--help' in sys.argv:
    print "Usage:"
    print " python %s prod <enableMask>" % (sys.argv[0])
    print " python %s test <sgf file> <crop> <enablePlay> <enableMask>" % (sys.argv[0])
    sys.exit(1)

if sys.argv[1] == 'prod':
    d3_js = read_content( 'd3.v3.min.js' )
    goban_js = read_content( 'goban.js' )
    sgf_js = read_content( 'sgf.js' )

    sgf_html = sgf_html.replace("{{Prescript}}", "")
    sgf_html = sgf_html.replace("{{D3Script}}", "<script>%s</script>" % (d3_js))
    sgf_html = sgf_html.replace("{{SGFScript}}", "<script>%s</script>" % (sgf_js))
    sgf_html = sgf_html.replace("{{GobanScript}}","<script>%s</script>" % (goban_js))
    sgf_html = sgf_html.replace("{{enableMask}}", sys.argv[2])


    sgf_html = sgf_html.replace("{{Initialize}}", "")

    o_html = 'card_front.html'

if sys.argv[1] == 'test':
    sgf_content = read_content(sys.argv[2])

    sgf_html = sgf_html.replace("{{Prescript}}", "<div id=\"back\">")
    sgf_html = sgf_html.replace("{{D3Script}}", '<script src="d3.v3.min.js"></script>')
    sgf_html = sgf_html.replace("{{SGFScript}}", '<script src="sgf.js"></script>')
    sgf_html = sgf_html.replace("{{GobanScript}}", '<script src="goban.js"></script>')

    if '--div' in sys.argv:
        sgf_html = sgf_html.replace("{{SGF}}", "<div>"+sgf_content.replace("\n", "</div><div>")+"</div>")
    else:
        sgf_html = sgf_html.replace("{{SGF}}", sgf_content)

    sgf_html = sgf_html.replace("{{Crop}}", sys.argv[3])
    sgf_html = sgf_html.replace("{{showDead}}", 'true')
    sgf_html = sgf_html.replace("{{enablePlay}}", sys.argv[4])
    sgf_html = sgf_html.replace("{{enableMask}}", sys.argv[5])

    if sys.argv[4] == 'true' or sys.argv[4] == 'True':
        sgf_html = sgf_html.replace("{{Initialize}}", "")
    else:
        sgf_html = sgf_html.replace("{{Initialize}}", "<script type=\"text/javascript\">var isBack=true;</script>")

#    sgf_html += """
#    <script>
#        d3.select(".sgfview").append("div").style('text-align', 'center').append("a").attr('href', '#').text('show beginning').on('click', function(){ board.toStart() });
#    </script>"""

    sgf_html = \
    """<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="sgf.css">
    </head>
    <body>
""" + sgf_html + """
    </body>
</html>"""

    o_html = 'test.html'

with open(o_html, 'w') as output:
    output.write(sgf_html)
