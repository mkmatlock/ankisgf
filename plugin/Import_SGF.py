from aqt import mw, deckchooser
from aqt.utils import showInfo
from aqt.qt import *
from aqt import tagedit
from go import gotools
from anki import notes

class ImportSGFDialog(QDialog):

    def __init__(self, mw, args):
        QDialog.__init__(self, None, Qt.Window)
        self.mw = mw
        self.setWindowTitle(_("Import SGF Files"))

        self.fileNames = args

        self.mainLayout = QVBoxLayout()
        self.makeFileCount()
        self.makeDeckInput()
        self.makeOptions()
        self.makeTagInput()
        self.makeButtons()

        self.setLayout( self.mainLayout )
        self.setGeometry(300, 300, 290, 150)
        self.show()

    def makeFileCount( self ):
        ql = QLabel( 'Importing %d files' % ( len(self.fileNames) ), self )
        self.mainLayout.addWidget( ql )

    def makeDeckInput( self ):
        deckArea = QWidget()
        self.deck = deckchooser.DeckChooser( self.mw, deckArea, label=True )
        self.mainLayout.addWidget(deckArea)

    def makeButtons( self ):
        self.okButton = QPushButton('Import')
        self.okButton.clicked.connect( self.finishImport )

        self.cancelButton = QPushButton('Cancel')
        self.cancelButton.clicked.connect( self.close )

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget( self.cancelButton )
        hbox.addWidget( self.okButton )

        vbox = QVBoxLayout()
        vbox.addStretch(1)
        vbox.addLayout(hbox)

        self.mainLayout.addLayout(vbox)

    def makeOptions( self ):
        vbox = QVBoxLayout()

        self.numberCheckBox = QCheckBox('Number terminal nodes')
        self.continuationCheckBox = QCheckBox('Branch after captures')
        self.showDeadStones = QCheckBox('Show dead stones')
        self.enableOpponent = QCheckBox('Enable opponent')

        vbox.addWidget( self.numberCheckBox )
        vbox.addWidget( self.continuationCheckBox )
        vbox.addWidget( self.showDeadStones )
        vbox.addWidget( self.enableOpponent )

        self.mainLayout.addLayout( vbox )

    def makeTagInput( self ):
        hbox = QHBoxLayout()
        self.tagEditor = tagedit.TagEdit( self )
        self.tagEditor.setCol( self.mw.col )
        text = QLabel( 'Tags', self )

        hbox.addWidget( text )
        hbox.addWidget( self.tagEditor )
        self.mainLayout.addLayout( hbox )

    def finishImport( self ):
        showDead = self.showDeadStones.isChecked()
        enablePlay = self.enableOpponent.isChecked()
        makeContinuation = self.continuationCheckBox.isChecked()
        addNumberings = self.numberCheckBox.isChecked()
        tagContent = self.tagEditor.text()

        failure=False

        self.close()

        tp = mw.col.models.byName("Goban")
        deck_id = self.deck.selectedId()

        self.mw.progress.start(max=len(self.fileNames), parent=self.mw, immediate=True)
        newCount = 0

        for fn in self.fileNames:
            sgf = gotools.import_sgf( fn )
            gotools.clean_sgf( sgf )

            if( makeContinuation ):
                gotools.split_continuations( sgf )
            if( addNumberings ):
                gotools.add_numberings( sgf )

            sgf_str = str( sgf )
            # Add card to cards
            note = notes.Note(mw.col, tp)
            note.model()['did'] = deck_id

            note['SGF'] = sgf_str
            note['showDead'] = str(showDead).lower()
            note['enablePlay'] = str(enablePlay).lower()
            note['Crop'] = gotools.get_crop( sgf )
            note.setTagsFromStr( tagContent )

            if not mw.col.addNote(note):
                failure=True
                break

            newCount += 1
            mw.progress.update(value=newCount)

        self.mw.progress.finish()
        self.mw.deckBrowser.refresh()
        self.mw.reset()

        if failure:
            QMessageBox.about(self.mw, "Import Failure", "Failed to import SGF files." )
        else:
            QMessageBox.about(self.mw, "Import Finished", "Successfully imported %d SGF files" % (newCount) )


def import_sgf():
    fileNames, _ = QFileDialog.getOpenFileNames(caption="Choose SGF Files...", filter="SGF Files (*.sgf)")
    if len(fileNames) > 0:
        mw.sgfDialog = ImportSGFDialog(mw, fileNames)


action = QAction("Import SGF Files...", mw)
action.triggered.connect(import_sgf)
mw.form.menuTools.addAction(action)
